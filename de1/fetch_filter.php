<?php
//fetch.php
if(isset($_POST["query"]))
{
 $connect = mysqli_connect("localhost", "root", "", "ponai");
 $request = mysqli_real_escape_string($connect, $_POST["query"]);
 $query = "
  SELECT * FROM patients 
  WHERE so_name LIKE '%".$request."%' 
  OR patient_name LIKE '%".$request."%' 
  OR prescription_dentist LIKE '%".$request."%' 
  OR refferal_dentist LIKE '%".$request."%' 
  OR date_stamp LIKE '%".$request."%' 
  OR diagnosis LIKE '%".$request."%'
 ";
 $result = mysqli_query($connect, $query);
 $data =array();
 $html = '';
 $html .= '
  <table class=" table table-striped table-bordered table-responsive table-hover">
   <tr>
   <th>Medical Insurance</th>
   <th>Prescription</th>
   <th>Referrals</th>
   <th>Patient Name</th>
   <th>Diagnosis</th>       
   <th>Visit Date</th>
 

    <th></th>
   </tr>
  ';
 if(mysqli_num_rows($result) > 0)
 {
  while($row = mysqli_fetch_array($result))
  {
   $data[] = $row["so_name"];
   $data[] = $row["prescription_dentist"];
   $data[] = $row["refferal_dentist"];
   $data[] = $row["patient_name"];
   $data[] = $row["diagnosis"];
   $data[] = $row["date_stamp"];
   $html .= '
   <tr>
    <td>'.$row["so_name"].'</td>
    <td>'.$row["prescription_dentist"].'</td>
    <td>'.$row["refferal_dentist"].'</td>
    <td>'.$row["patient_name"].'</td>
    <td>'.$row["diagnosis"].'</td>
    <td>'.$row["date_stamp"].'</td>
	<td><a href="expand_patient.php?id='.$row['id'].'" class="btn btn-success">Expand Record</a></td>
   
   </tr>
   ';
  }
 }
 else
 {
  $data = 'No Data Found';
  $html .= '
   <tr>
    <td colspan="3">No Data Found</td>
   </tr>
   ';
 }
 $html .= '</table>';
 if(isset($_POST['typehead_search']))
 {
  echo $html;
 }
 else
 {
  $data = array_unique($data);
  echo json_encode($data);
 }
}

?>