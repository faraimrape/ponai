-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 02, 2018 at 01:15 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ponai`
--

-- --------------------------------------------------------

--
-- Table structure for table `patients`
--

CREATE TABLE `patients` (
  `user_id` mediumint(10) NOT NULL,
  `so_name` varchar(40) NOT NULL,
  `me_name` varchar(40) NOT NULL,
  `po_address` varchar(80) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `idnumber` varchar(10) NOT NULL,
  `name_employer` varchar(40) NOT NULL,
  `patient_name` varchar(40) NOT NULL,
  `rel_member` varchar(20) NOT NULL,
  `member_num` varchar(20) NOT NULL,
  `member_suffix` varchar(20) NOT NULL,
  `patient_dob` varchar(10) NOT NULL,
  `claim_number` varchar(10) NOT NULL,
  `date_stamp` varchar(10) NOT NULL,
  `roadtraffic` varchar(40) NOT NULL,
  `accidentwork` varchar(40) NOT NULL,
  `accidenthome` varchar(40) NOT NULL,
  `other_specify` varchar(30) NOT NULL,
  `sig_fullname1` varchar(40) NOT NULL,
  `sig_fullname2` varchar(40) NOT NULL,
  `sig_fullname3` varchar(40) NOT NULL,
  `sig_fullname4` varchar(40) NOT NULL,
  `sig_fullname5` varchar(40) NOT NULL,
  `sig_fullname6` varchar(40) NOT NULL,
  `sig_date1` varchar(10) NOT NULL,
  `sig_date2` varchar(10) NOT NULL,
  `sig_date3` varchar(10) NOT NULL,
  `sig_date4` varchar(10) NOT NULL,
  `sig_date5` varchar(10) NOT NULL,
  `sig_date6` varchar(10) NOT NULL,
  `sig_relationship1` varchar(20) NOT NULL,
  `sig_relationship2` varchar(20) NOT NULL,
  `sig_relationship3` varchar(20) NOT NULL,
  `sig_relationship4` varchar(20) NOT NULL,
  `sig_relationship5` varchar(20) NOT NULL,
  `sig_relationship6` varchar(20) NOT NULL,
  `sig_fee1` varchar(10) NOT NULL,
  `sig_fee2` varchar(10) NOT NULL,
  `sig_fee3` varchar(10) NOT NULL,
  `sig_fee4` varchar(10) NOT NULL,
  `sig_fee5` varchar(10) NOT NULL,
  `sig_fee6` varchar(10) NOT NULL,
  `ahfoz` varchar(20) NOT NULL,
  `refpracti` varchar(30) NOT NULL,
  `dateclosed` varchar(10) NOT NULL,
  `anesthetist` varchar(30) NOT NULL,
  `accountref` varchar(30) NOT NULL,
  `surgical_ass` varchar(40) NOT NULL,
  `rel_ahfoz1` varchar(20) NOT NULL,
  `rel_ahfoz2` varchar(20) NOT NULL,
  `rel_ahfoz3` varchar(20) NOT NULL,
  `tariff1` varchar(10) NOT NULL,
  `tariff2` varchar(10) NOT NULL,
  `tariff3` varchar(10) NOT NULL,
  `tariff4` varchar(10) NOT NULL,
  `tariff5` varchar(10) NOT NULL,
  `mod1a` varchar(10) NOT NULL,
  `mod1b` varchar(10) NOT NULL,
  `mod1c` varchar(10) NOT NULL,
  `mod1d` varchar(10) NOT NULL,
  `mod1e` varchar(10) NOT NULL,
  `qty1` varchar(10) NOT NULL,
  `qty2` varchar(10) NOT NULL,
  `qty3` varchar(10) NOT NULL,
  `qty4` varchar(10) NOT NULL,
  `qty5` varchar(10) NOT NULL,
  `date1` varchar(10) NOT NULL,
  `date2` varchar(10) NOT NULL,
  `date3` varchar(10) NOT NULL,
  `date4` varchar(10) NOT NULL,
  `date5` varchar(10) NOT NULL,
  `fee1` varchar(10) NOT NULL,
  `fee2` varchar(10) NOT NULL,
  `fee3` varchar(10) NOT NULL,
  `fee4` varchar(10) NOT NULL,
  `fee5` varchar(10) NOT NULL,
  `description1` varchar(70) NOT NULL,
  `description2` varchar(70) NOT NULL,
  `description3` varchar(70) NOT NULL,
  `description4` varchar(70) NOT NULL,
  `description5` varchar(70) NOT NULL,
  `grosstotal` varchar(10) NOT NULL,
  `diagnosis` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patients`
--

INSERT INTO `patients` (`user_id`, `so_name`, `me_name`, `po_address`, `phone`, `idnumber`, `name_employer`, `patient_name`, `rel_member`, `member_num`, `member_suffix`, `patient_dob`, `claim_number`, `date_stamp`, `roadtraffic`, `accidentwork`, `accidenthome`, `other_specify`, `sig_fullname1`, `sig_fullname2`, `sig_fullname3`, `sig_fullname4`, `sig_fullname5`, `sig_fullname6`, `sig_date1`, `sig_date2`, `sig_date3`, `sig_date4`, `sig_date5`, `sig_date6`, `sig_relationship1`, `sig_relationship2`, `sig_relationship3`, `sig_relationship4`, `sig_relationship5`, `sig_relationship6`, `sig_fee1`, `sig_fee2`, `sig_fee3`, `sig_fee4`, `sig_fee5`, `sig_fee6`, `ahfoz`, `refpracti`, `dateclosed`, `anesthetist`, `accountref`, `surgical_ass`, `rel_ahfoz1`, `rel_ahfoz2`, `rel_ahfoz3`, `tariff1`, `tariff2`, `tariff3`, `tariff4`, `tariff5`, `mod1a`, `mod1b`, `mod1c`, `mod1d`, `mod1e`, `qty1`, `qty2`, `qty3`, `qty4`, `qty5`, `date1`, `date2`, `date3`, `date4`, `date5`, `fee1`, `fee2`, `fee3`, `fee4`, `fee5`, `description1`, `description2`, `description3`, `description4`, `description5`, `grosstotal`, `diagnosis`) VALUES
(1, '$so_name', '$me_name', '$po_address', '$phone', '$idnumber', '$name_employer', '$patient_name', '$rel_member', '$member_num', '$member_su', '$patient_d', '$claim_num', '$date_stam', '$roadtraffic', '$accidentwork', '$accidenthome', '$other_specify', '$sig_fullname1', '$sig_fullname2', '$sig_fullname3', '$sig_fullname4', '$sig_fullname5', '$sig_fullname6', '$sig_date1', '$sig_date2', '$sig_date3', '$sig_date4', '$sig_date5', '$sig_date6', '$sig_relationship1', '$sig_relationship2', '$sig_relationship3', '$sig_relationship4', '$sig_relationship5', '$sig_relationship6', '$sig_fee1', '$sig_fee2', '$sig_fee3', '$sig_fee4', '$sig_fee5', '$sig_fee6', '$ahfoz', '$refpracti', '$dateclose', '$anesthetist', '$accountref', '$surgical_ass', '$rel_ahfoz1', '$rel_ahfoz2', '$rel_ahfoz3', '$tariff1', '$tariff2', '$tariff3', '$tariff4', '$tariff5', '$mod1a', '$mod1b', '$mod1c', '$mod1d', '$mod1e', '$qty1', '$qty2', '$qty3', '$qty4', '$qty5', '$date1', '$date2', '$date3', '$date4', '$date5', '$fee1', '$fee2', '$fee3', '$fee4', '$fee5', '$description1', '$description2', '$description3', '$description4', '$description5', '$grosstota', '$diagnosis'),
(2, 'PSMAS', 'Farai Mrape', 'ABC', '0774449714', '632058080B', 'TTCS Global', 'Anesu ', 'brother', '5945838', '02', '1994-01-04', '2248', '2018.09.01', 'Road traffic accident', 'Accident at work', 'Accident at home', 'NA', 'ABC', 'aaydag', 'aydgagd', 'ygda', 'ydausgdy', 'dygs', 'dyqd', 'dqyyudd', 'fauyef', 'fuayfg', 'fqygf', 'yaufg', 'yusgu', 'ydagv', 'yuvgwuyv', 'qcqiygu', 'gcu', 'fgv', 'ugucuycgu', 'gu', 'dgcuycguyc', 'uuyffuyw', 'cuyqgc', 'v', 'iisdfusi', 'dfiugsui', 'fuqufg', 'iuqiufgqi', 'iuvqiuvg', 'iucg', 'ivg', 'kvg', 'ivg', 'ivg', 'iugvi', 'vgi', 'fvg', 'ifvg', 'ivggi', 'uvggiu', 'vg', 'ivg', 'givg', '1', '34', '4', '3', '4', 'iuvgvvgiu', 'ugvi', 'gvi', 'vgi', 'vg', 'ivdorfhsk', 'usuii', 'iwuvwig', 'gsigvwi', 'gvsiug', 'uivgsiugu', 'vus', 'igvqi', 'gciqgqig', 'ic', 'igu', 'gic '),
(3, 'PSMAS', 'Farai Mrape', 'ABC', '0774449714', '632058080B', 'TTCS Global', 'Anesu ', 'brother', '5945838', '02', '1994-01-04', '2248', '2018.09.01', 'Road traffic accident', 'Accident at work', 'Accident at home', 'NA', 'ABC', 'aaydag', 'aydgagd', 'ygda', 'ydausgdy', 'dygs', 'dyqd', 'dqyyudd', 'fauyef', 'fuayfg', 'fqygf', 'yaufg', 'yusgu', 'ydagv', 'yuvgwuyv', 'qcqiygu', 'gcu', 'fgv', 'ugucuycgu', 'gu', 'dgcuycguyc', 'uuyffuyw', 'cuyqgc', 'v', 'iisdfusi', 'dfiugsui', 'fuqufg', 'iuqiufgqi', 'iuvqiuvg', 'iucg', 'ivg', 'kvg', 'ivg', 'ivg', 'iugvi', 'vgi', 'fvg', 'ifvg', 'ivggi', 'uvggiu', 'vg', 'ivg', 'givg', '1', '34', '4', '3', '4', 'iuvgvvgiu', 'ugvi', 'gvi', 'vgi', 'vg', 'ivdorfhsk', 'usuii', 'iwuvwig', 'gsigvwi', 'gvsiug', 'uivgsiugu', 'vus', 'igvqi', 'gciqgqig', 'ic', 'igu', 'gic ');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `status` enum('1','0') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `secret` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `phone`, `created`, `modified`, `status`, `secret`, `role`) VALUES
(1, 'Farai', 'Mrape', 'flipcollins@yahoo.co.uk', '827ccb0eea8a706c4c34a16891f84e7b', '+264774449715', '2018-08-23 13:12:01', '2018-08-23 13:12:01', '1', '12345', 'Superuser'),
(10, 'Anesu', 'Mrape', 'anesu@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '+264774449714', '2018-08-26 13:46:08', '2018-08-26 13:46:08', '1', '21wxds43', 'Doctor'),
(11, 'Test', 'User', 'test@test.com', 'b2ead5e75fe0f62676970a2fca155a03', '+264774449714', '2018-08-26 15:01:50', '2018-08-26 15:01:50', '1', '122332D', 'Dentist'),
(12, 'John', 'Doe', 'johndoe@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '07475399', '2018-08-30 14:33:26', '2018-08-30 14:33:26', '1', 'iamlegend', 'Receptionist');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `patients`
--
ALTER TABLE `patients`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `secret` (`secret`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `patients`
--
ALTER TABLE `patients`
  MODIFY `user_id` mediumint(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
