<?php
session_start();
$sessData = !empty($_SESSION['sessData'])?$_SESSION['sessData']:'';
if(!empty($sessData['status']['msg'])){
    $statusMsg = $sessData['status']['msg'];
    $statusMsgType = $sessData['status']['type'];
    unset($_SESSION['sessData']['status']);
    
}
if(!empty($sessData['userLoggedIn']) && !empty($sessData['userID'])){
    include 'user.php';
    $user = new User();
    $conditions['where'] = array(
        'id' => $sessData['userID'],
    );
    $conditions['return_type'] = 'single';
    $userData = $user->getRows($conditions);
}else{
    header("Location: ../index.php");
}

?>
<!DOCTYPE html>
<html>
 <head>
 <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="assets/fullcalendar.css" />
  <link rel="stylesheet" href="assets/bootstrap.css" />
  <script src="assets/jquery-1.11.1.min.js"></script>
  <script src="assets/jquery-ui.min.js"></script>
  <script src="assets/moment.min.js"></script>
  <script src="assets/fullcalendar.min.js"></script>
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="assets/css/style.css">
  <link rel="stylesheet" href="assets/jquery-ui.css">
  <script>
   
  $(document).ready(function() {
   var calendar = $('#calendar').fullCalendar({
    editable:true,
    header:{
     left:'prev,next today',
     center:'title',
     right:'month,agendaWeek,agendaDay'
    },
    events: 'load.php',
    selectable:true,
    selectHelper:true,
    select: function(start, end, allDay)
    {
     var title = prompt("Enter Event Title");
     if(title)
     {
      var start = $.fullCalendar.formatDate(start, "Y-MM-DD HH:mm:ss");
      var end = $.fullCalendar.formatDate(end, "Y-MM-DD HH:mm:ss");
      $.ajax({
       url:"insert.php",
       type:"POST",
       data:{title:title, start:start, end:end},
       success:function()
       {
        calendar.fullCalendar('refetchEvents');
        alert("Added Successfully");
       }
      })
     }
    },
    editable:true,
    eventResize:function(event)
    {
     var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
     var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
     var title = event.title;
     var id = event.id;
     $.ajax({
      url:"update.php",
      type:"POST",
      data:{title:title, start:start, end:end, id:id},
      success:function(){
       calendar.fullCalendar('refetchEvents');
       alert('Event Update');
      }
     })
    },

    eventDrop:function(event)
    {
     var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
     var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
     var title = event.title;
     var id = event.id;
     $.ajax({
      url:"update.php",
      type:"POST",
      data:{title:title, start:start, end:end, id:id},
      success:function()
      {
       calendar.fullCalendar('refetchEvents');
       alert("Event Updated");
      }
     });
    },

    eventClick:function(event)
    {
     if(confirm("Are you sure you want to remove it?"))
     {
      var id = event.id;
      $.ajax({
       url:"delete.php",
       type:"POST",
       data:{id:id},
       success:function()
       {
        calendar.fullCalendar('refetchEvents');
        alert("Event Removed");
       }
      })
     }
    },

   });
  });
   
  </script>
  <title>Reception Area</title>
 </head>
<body>


  <div class="container">
  <div class="row">
  </div>
  <div class="row">
<div class="form-box">
<div class="form-top">
<div class="form">
<br />
<a class="btn btn-link-2" href="calendar.php">
Schedule appointments
</a>
<a class="btn btn-link-2" href="index.php">
View patient visits
</a>
<a class="btn btn-link-2" href="create_visit.php">
Create patient visits
</a>
<a class="btn btn-link-2" href="filter.php">
Filter patients
</a> 
<a class="btn btn-link-2" href="backup.php">
Check card status
</a>

<a href="../userAccount.php?logoutSubmit=1" class="btn btn-link-2" >Logout</a>
</div>
</div>
<div class="form-bottom">
         <style>
            .my_text
            {
                font-family:    "Helvetica Neue", Helvetica, Arial, sans-serif;
                font-size:      30px;
                font-weight:    bold;
            }
        </style>
 <div class="alert alert-info" >
<div class="row">   

   <div id="calendar" class="my_text"></div>
  </div>
</div>
  <div class="alert alert-info" >
<div class="row">
<div class="col-sm-2"><img src = "assets/img/backgrounds/iconlist.png" height="90" width="80" class="img-responsive"></div>
<div class="col-sm-8"></div>
<div class="col-sm-2">
<font size="-1">
<b>Name</b> - <?php echo $userData['first_name']; ?><br />
<b>Surname </b> - <?php echo $userData['last_name']; ?> <br />
<b> Role  </b> - <?php echo $userData['role']; ?>
</font>
</div>



</div>

</div>

</div>
</div>
</div>
<div class="row">

</div>
</div>
</div>

<!-- Javascript -->

<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/js/jquery.backstretch.min.js"></script>
<script src="assets/js/scripts.js"></script>

<!--[if lt IE 10]>
<script src="assets/js/placeholder.js"></script>
<![endif]-->

</body>

</html>