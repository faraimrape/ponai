<?php
//fetch.php
if(isset($_POST["query"]))
{
 $connect = mysqli_connect("localhost", "root", "", "ponai");
 $request = mysqli_real_escape_string($connect, $_POST["query"]);
 $query = "
  SELECT * FROM patients 
  WHERE so_name LIKE '%".$request."%' 
  OR patient_name LIKE '%".$request."%' 
  OR me_name LIKE '%".$request."%' 
  OR claim_number LIKE '%".$request."%' 
  OR date_stamp LIKE '%".$request."%' 
  OR idnumber LIKE '%".$request."%'
 ";
 $result = mysqli_query($connect, $query);
 $data =array();
 $html = '';
 $html .= '
  <table class="table table-bordered table-striped">
   <tr>
    <th>Society Name</th>
    <th>Member Name</th>
    <th>Member Number</th>
    <th>Patient Name</th>
    <th>Relationship</th>
    <th>Claim Number</th>
    <th>Date</th>
	 <th></th>
    <th></th>
   </tr>
  ';
 if(mysqli_num_rows($result) > 0)
 {
  while($row = mysqli_fetch_array($result))
  {
   $data[] = $row["so_name"];
   $data[] = $row["me_name"];
   $data[] = $row["member_num"];
   $data[] = $row["patient_name"];
   $data[] = $row["rel_member"];
   $data[] = $row["claim_number"];
   $data[] = $row["date_stamp"];
   $html .= '
   <tr>
    <td>'.$row["so_name"].'</td>
    <td>'.$row["me_name"].'</td>
    <td>'.$row["member_num"].'</td>
    <td>'.$row["patient_name"].'</td>
    <td>'.$row["rel_member"].'</td>
    <td>'.$row["claim_number"].'</td>
    <td>'.$row["date_stamp"].'</td>
	<td><a href="expand_patient.php?id='.$row['id'].'" class="btn btn-success">Expand Record</a></td>
    <td><a href="deleterecord.php?id='.$row['id'].'" class="btn btn-danger">Delete</a></td>
   </tr>
   ';
  }
 }
 else
 {
  $data = 'No Data Found';
  $html .= '
   <tr>
    <td colspan="3">No Data Found</td>
   </tr>
   ';
 }
 $html .= '</table>';
 if(isset($_POST['typehead_search']))
 {
  echo $html;
 }
 else
 {
  $data = array_unique($data);
  echo json_encode($data);
 }
}

?>